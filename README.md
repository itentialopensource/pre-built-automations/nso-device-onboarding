## _Deprecation Notice_
This Pre-Built has been deprecated as of 04-30-2024 and will be end of life on 04-30-2025. The capabilities of this Pre-Built have been replaced by the [Cisco - NSO](https://gitlab.com/itentialopensource/pre-built-automations/cisco-nso)

# NSO Device Onboarding

## Table of Contents

- [NSO Device Onboarding](#nso-device-onboarding)
  - [Table of Contents](#table-of-contents)
  - [Overview](#overview)
    - [Capabilities](#capabilities)
  - [Getting Started](#getting-started)
    - [Supported IAP Versions](#supported-iap-versions)
    - [Known Limitations](#known-limitations)
    - [External Dependencies](#external-dependencies)
    - [Adapters](#adapters)
    - [How to Install](#how-to-install)
    - [Testing](#testing)
  - [Using this Pre-Built Automation](#using-this-pre-built-automation)
    - [Entry Point IAP Component](#entry-point-iap-component)
    - [Inputs](#inputs)
    - [Outputs](#outputs)
    - [Error Handling](#error-handling)
  - [Support](#support)

## Overview

The `NSO Device Onboarding` Pre-Built Automation onboards one  device or multiple devices into NSO instance. Once the user has provided all of the required parameters and chooses to continue, the workflow will begin to onboard each of the provided devices.

### Capabilities

- This prebuilt allows a user to onboard devices into an NSO instance.
- Can `add` a device into the NSO database
- Perform a `fetch` on the device SSH keys (if applicable)
- Execute a `sync-from` on the device being onboarded
- If `autoApprove` flag is enabled then at the end of the workflow, the user will be presented with a comprehensive report that lists the following:
  - Devices that were successfully onboarded 
  - Devices that were not added to the NSO database 
  - Devices that failed Sync-From 
  - Devices that failed to fetch SSH keys (if applicable)

## Getting Started

### Supported IAP Versions

Itential Pre-Built Automations are built and tested on particular versions of IAP. In addition, Pre-Built Automations that work with devices are often dependent on certain orchestration systems (e.g. NSO and IAG). As such, these Pre-Built Automations will have dependencies on these other systems. This version of the `NSO Device Onboarding` Pre-Built Automation has been tested with:

- IAP 2023.1

### External Dependencies

| Dependency Name | Dependency Version |
|---------------- |--------------------|
| NSO | ^5.7.8|

### Adapters

No external adapters required to run this Pre-Built Automation.

### How to Install

To install the Workflow Project:

- Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Supported IAP Versions](#supported-iap-versions) section in order to install the Workflow Project.
- Import the Workflow Project in [Admin Essentials](https://docs.itential.com/docs/importing-a-prebuilt-4).


### Testing

While Itential tests this Pre-Built Automation and its capabilities, it is often the case the customer environments offer their own unique circumstances. Therefore, it is our recommendation that you deploy this Pre-Built Automation into a development/testing environment in which you can test the Pre-Built Automation.

## Using this Pre-Built Automation

### Entry Point IAP Component

The primary IAP component to run this Pre-Built Automation is listed below:

| IAP Component Name | IAP Component Type |
|------------------- |--------------------|
| IAP NSO Device Onboarding | Operations Manager Automation |

### Inputs

The following is an example input to this Pre-Built Automation:

```json
{
  "formData": {
    "deviceS": [
      {
        "nsoInstance": "nso57",
        "name": "device1",
        "address": "10.10.10.10",
        "port": "22",
        "authgroup": "authgroupName",
        "deviceType": "cli",
        "nedId": "alu-sr-cli-8.32",
        "adminState": "unlocked",
        "protocol": "ssh"
      }
    ],
    "autoApprove": false
  }
}
```
The following table lists the inputs to the Pre-Built Automation:

| Name                | Type             | Required                | Description          | Example Value |
|------------------- |------------------|-------------------------|----------------------|------------------------|
| formData.deviceS[0].nsoInstance     | string   | yes      | The NSO server | nso57 |
| formData.deviceS[0].name     | string   | yes      | The name of the device | device1 |
| formData.deviceS[0].address     | string   | yes      | IP address or host name for the management interface | 10.10.10.10 |
| formData.deviceS[0].port     | string   | yes      | Port for the management interface | 22 |
| formData.deviceS[0].authgroup     | string   | yes      | Authentication credentials for the device | admin |
| formData.deviceS[0].deviceType     | string   | yes      | Management protocol for the device | cli |
| formData.deviceS[0].nedId     | string   | yes      | The NED identity | alu-sr-cli-8.32 |
| formData.deviceS[0].adminState     | string   | yes      | The admin state of the device | unlocked |
| formData.deviceS[0].protocol     | string   | yes      | THE CLI protocol | ssh |
| formData.autoApprove     | boolean   | yes      | if set to true, will not display any manual tasks | true |

### Outputs

The following is an example output of this Pre-Built Automation:

```json
{
  "devicesAddedSuccessfully": [
    "onboarding-test"
  ],
  "devicesFetchedNotSynced": [],
  "devicesAddednotFetched": [],
  "devicesAddedFailure": []
}
```
The following table lists the outputs of the Pre-Built Automation:

| Name                | Type             | Description          | Example Value |
|------------------- |------------------|----------------------|------------------------|
| devicesAddedSuccessfully     | array   | List of device names that were successfully onboarded |  |
| devicesAddednotFetched     | array   | List of device names that are added but failed to fetch SSH keys (if applicable) |  |
| devicesAddedFailure     | array   | List of device names that were not added to the NSO database |  |
| devicesFetchedNotSynced     | array   | List of device names that are failed Sync-From |  |

#### Query Output

The following is how to query successful results from the output:

##### devicesAddedSuccessfully

`devicesAddedSuccessfully`

The following is how to query unsuccessful results from the output:

##### devicesAddednotFetched

`devicesAddednotFetched`

##### devicesAddedFailure

`devicesAddedFailure`

##### devicesFetchedNotSynced

`devicesFetchedNotSynced`

#### Error Handling
As mentioned above, NSO Device Onboarding will provide the user with a report that shows devices that were onboarded successfully and any device that failed the onboarding process. If a device fails to be added to the NSO database, fails to fetch SSH keys, or fails sync-from it will be caught and outputted to the user. At each step in the onboarding process a device will be evaluated in terms of erroring, failure, or success. Upon error or failure, the device will be added to a list corresponding to the step in the onboarding process.

## Support

Please use your Itential Customer Success account if you need support when using this Pre-Built Automation.