
## 0.2.10 [07-12-2024]

Add deprecation notice and metadata

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!52

2024-07-12 19:40:13 +0000

---

## 0.2.9 [07-21-2023]

* Updates output data of device onboarding results, adds Cypress tests, and enforces a minimum of one device be provided for Operations Manager form.

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!50

---

## 0.2.8 [07-05-2023]

* Added Auto Approve flag to bypass manual step

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!47

---

## 0.2.7 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!44

---

## 0.2.6 [06-21-2022]

* patch/DSUP-1361

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!43

---

## 0.2.5 [01-05-2022]

* Certify on 2021.2

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!40

---

## 0.2.4 [07-02-2021]

* Update package.json, README.md files

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!39

---

## 0.2.3 [03-04-2021]

* Patch/lb 515 master

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!38

---

## 0.2.2 [08-19-2020]

* Updated README.md for IAP version requirement.

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!37

---

## 0.2.1 [08-14-2020]

* Update NSO Device Onboarding Artifact to use JSTs

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!36

---

## 0.2.0 [07-16-2020]

* [minor/LB-404] Switch absolute path to relative path

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!34

---

## 0.1.0 [06-17-2020]

* Update manifest and readme to reflect gitlab name

See merge request itentialopensource/pre-built-automations/NSO-Device-Onboarding!33

---

## 0.0.12 [05-27-2020]

* Update bundles/json_forms/IAP NSO Device Onboarding.json,...

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!32

---

## 0.0.11 [05-13-2020]

* [patch/IPSO-3685] Update bundles/workflows/IAP NSO Device Onboarding.json

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!29

---

## 0.0.10 [04-17-2020]

* Update IAPDependencies and removed app-artifacts as dependency

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!27

---

## 0.0.9 [04-17-2020]

* Update IAPDependencies and removed app-artifacts as dependency

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!27

---

## 0.0.8 [04-14-2020]

* Update IAPDependencies and removed app-artifacts as dependency

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!27

---

## 0.0.7 [03-30-2020]

* removed version from manifest.json

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!25

---

## 0.0.6 [03-10-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!22

---

## 0.0.5 [02-27-2020]

* Updated master with 2019.3 release

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!19

---

## 0.0.4 [02-27-2020]

* Updated master with 2019.3 release

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!19

---

## 0.0.3 [02-26-2020]

* Add artifacts keyword

See merge request itentialopensource/pre-built-automations/nso-device-onboarding!20

---

## 0.0.2 [01-27-2020]

* update from Itential Artifacts

See merge request itentialopensource/pre-built-automations/staging/nso-device-onboarding!18

---

## 0.0.2-2019.3.4 [02-03-2020]

* update from Itential Artifacts

See merge request itentialopensource/pre-built-automations/staging/nso-device-onboarding!18

---

## 0.0.2-2019.3.3 [02-03-2020]

* update from Itential Artifacts

See merge request itentialopensource/pre-built-automations/staging/nso-device-onboarding!18

---

## 0.0.2-2019.3.2 [02-03-2020]

* update from Itential Artifacts

See merge request itentialopensource/pre-built-automations/staging/nso-device-onboarding!18

---

## 0.0.2-2019.3.1 [02-03-2020]

* update from Itential Artifacts

See merge request itentialopensource/pre-built-automations/staging/nso-device-onboarding!18

---

## 0.0.2-2019.3.0 [02-03-2020]

* update from Itential Artifacts

See merge request itentialopensource/pre-built-automations/staging/nso-device-onboarding!18

---

## 0.0.2 [01-27-2020]

* update from Itential Artifacts

See merge request itentialopensource/pre-built-automations/staging/nso-device-onboarding!18

---\n\n\n\n
